type bool_expr =
  | Var of string
  | Not of bool_expr
  | And of bool_expr * bool_expr
  | Or of bool_expr * bool_expr

(* 46 & 47. Truth tables for logical expressions (2 variables). (medium) *)
let rec eval_expr2 a b a_val b_val = function
  | Var x -> if x = a then a_val else if x = b then b_val else failwith "Invalid x"
  | Not e -> not (eval_expr2 a b a_val b_val e)
  | And (e1, e2) -> eval_expr2 a b a_val b_val e1 && eval_expr2 a b a_val b_val e2
  | Or (e1, e2) -> eval_expr2 a b a_val b_val e1 || eval_expr2 a b a_val b_val e2
;;

let table2 a b e =
  [ true, true, eval_expr2 a b true true e
  ; true, false, eval_expr2 a b true false e
  ; false, true, eval_expr2 a b false true e
  ; false, false, eval_expr2 a b false false e
  ]
;;

let () =
  assert (
    table2 "a" "b" (And (Var "a", Or (Var "a", Var "b")))
    = [ true, true, true; true, false, true; false, true, false; false, false, false ])
;;

(* 48. Truth tables for logical expressions. (medium) *)
let rec eval_expr assoc_list = function
  | Var x -> List.assoc x assoc_list
  | Not e -> not (eval_expr assoc_list e)
  | And (e1, e2) -> eval_expr assoc_list e1 && eval_expr assoc_list e2
  | Or (e1, e2) -> eval_expr assoc_list e1 || eval_expr assoc_list e2
;;

let table variables expr =
  let rec aux assoc_list = function
    | [] -> [ List.rev assoc_list, eval_expr assoc_list expr ]
    | h :: t ->
      let true_case = aux ((h, true) :: assoc_list) t in
      let false_case = aux ((h, false) :: assoc_list) t in
      true_case @ false_case
  in
  aux [] variables
;;

let () =
  assert (
    table [ "a"; "b" ] (And (Var "a", Or (Var "a", Var "b")))
    = [ [ "a", true; "b", true ], true
      ; [ "a", true; "b", false ], true
      ; [ "a", false; "b", true ], false
      ; [ "a", false; "b", false ], false
      ]);
  let a = Var "a"
  and b = Var "b"
  and c = Var "c" in
  assert (
    table [ "a"; "b"; "c" ] (Or (And (a, Or (b, c)), Or (And (a, b), And (a, c))))
    = [ [ "a", true; "b", true; "c", true ], true
      ; [ "a", true; "b", true; "c", false ], true
      ; [ "a", true; "b", false; "c", true ], true
      ; [ "a", true; "b", false; "c", false ], false
      ; [ "a", false; "b", true; "c", true ], false
      ; [ "a", false; "b", true; "c", false ], false
      ; [ "a", false; "b", false; "c", true ], false
      ; [ "a", false; "b", false; "c", false ], false
      ])
;;

(* 49. Gray code. (medium) *)
let rec gray n =
  if n <= 0
  then []
  else if n = 1
  then [ "0"; "1" ]
  else (
    let prev_gray = gray (n - 1) in
    let gray_rev = List.rev prev_gray in
    let add_prefix prefix lst = List.map (fun s -> prefix ^ s) lst in
    add_prefix "0" prev_gray @ add_prefix "1" gray_rev)
;;

let () =
  assert (gray 1 = [ "0"; "1" ]);
  assert (gray 2 = [ "00"; "01"; "11"; "10" ]);
  assert (gray 3 = [ "000"; "001"; "011"; "010"; "110"; "111"; "101"; "100" ])
;;

(* 50. Huffman code (hard) *)
type 'a tree =
  | Leaf of 'a
  | Node of 'a tree * 'a tree

module HSet = Set.Make (struct
    type t = int * string tree

    let compare = compare
  end)

let build_tree hist =
  let leaves = HSet.of_list (List.map (fun (s, f) -> f, Leaf s) hist) in
  let rec aux trees =
    let f1, a = HSet.min_elt trees in
    let trees' = HSet.remove (f1, a) trees in
    if HSet.is_empty trees'
    then a
    else (
      let f2, b = HSet.min_elt trees' in
      let trees'' = HSet.remove (f2, b) trees' in
      let trees''' = HSet.add (f1 + f2, Node (a, b)) trees'' in
      aux trees''')
  in
  aux leaves
;;

let rec build_prefixes prefix = function
  | Leaf x -> [ x, prefix ]
  | Node (left, right) ->
    build_prefixes (prefix ^ "0") left @ build_prefixes (prefix ^ "1") right
;;

let huffman hist =
  let tree_ = build_tree hist in
  build_prefixes "" tree_
;;

let fs = [ "a", 45; "b", 13; "c", 12; "d", 16; "e", 9; "f", 5 ]

let () =
  assert (
    huffman fs
    = [ "a", "0"; "c", "100"; "b", "101"; "f", "1100"; "e", "1101"; "d", "111" ])
;;
