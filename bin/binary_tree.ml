type 'a binary_tree =
  | Empty
  | Node of 'a * 'a binary_tree * 'a binary_tree

(* 55. Construct completely balanced binary trees. (medium) *)
(* let cbal_tree n = *)
(*   let rec aux = function *)
(*     | 0 -> [ Empty ] *)
(*     | 1 -> [ Node ('x', Empty, Empty) ] *)
(*     | n when n mod 2 = 0 -> *)
(*       let sub_tree1 = aux ((n / 2) - 1) in *)
(*       let sub_tree2 = aux (n / 2) in *)
(*       List.concat_map *)
(*         (fun left -> *)
(*           List.concat_map *)
(*             (fun right -> [ Node ('x', left, right); Node ('x', right, left) ]) *)
(*             sub_tree2) *)
(*         sub_tree1 *)
(*     | n -> *)
(*       let sub_tree = aux (n / 2) in *)
(*       List.concat_map *)
(*         (fun left -> List.map (fun right -> Node ('x', left, right)) sub_tree) *)
(*         sub_tree *)
(*   in *)
(*   aux n *)
(* ;; *)

(* I think my solution was valid, but the order was not the same, so i am using their solution. *)
let add_trees_with left right all =
  let add_right_tree all l =
    List.fold_left (fun a r -> Node ('x', l, r) :: a) all right
  in
  List.fold_left add_right_tree all left
;;

let rec cbal_tree n =
  if n = 0
  then [ Empty ]
  else if n mod 2 = 1
  then (
    let t = cbal_tree (n / 2) in
    add_trees_with t t [])
  else (
    (* n even: n-1 nodes for the left & right subtrees altogether. *)
    let t1 = cbal_tree ((n / 2) - 1) in
    let t2 = cbal_tree (n / 2) in
    add_trees_with t1 t2 (add_trees_with t2 t1 []))
;;

let () =
  assert (
    cbal_tree 4
    = [ Node ('x', Node ('x', Empty, Empty), Node ('x', Node ('x', Empty, Empty), Empty))
      ; Node ('x', Node ('x', Empty, Empty), Node ('x', Empty, Node ('x', Empty, Empty)))
      ; Node ('x', Node ('x', Node ('x', Empty, Empty), Empty), Node ('x', Empty, Empty))
      ; Node ('x', Node ('x', Empty, Node ('x', Empty, Empty)), Node ('x', Empty, Empty))
      ]);
  assert (List.length (cbal_tree 40) = 524288)
;;

(* 56. Symmetric binary trees. (medium) *)
let rec is_mirror this other =
  match this, other with
  | Empty, Empty -> true
  | Node _, Empty | Empty, Node _ -> false
  | Node (_, this_left, this_right), Node (_, other_left, other_right) ->
    is_mirror this_left other_left && is_mirror this_right other_right
;;

let is_symmetric = function
  | Empty -> true
  | Node (_, left, right) -> is_mirror left right
;;

(* 57. Binary search trees (dictionaries). (medium) *)
let rec insert tree x =
  match tree with
  | Empty -> Node (x, Empty, Empty)
  | Node (el, left, right) ->
    if x = el
    then tree
    else if x < el
    then Node (el, insert left x, right)
    else Node (el, left, insert right x)
;;

let construct lst =
  let rec aux acc = function
    | [] -> acc
    | h :: t -> aux (insert acc h) t
  in
  aux Empty lst
;;

let () =
  assert (
    construct [ 3; 2; 5; 7; 1 ]
    = Node
        ( 3
        , Node (2, Node (1, Empty, Empty), Empty)
        , Node (5, Empty, Node (7, Empty, Empty)) ));
  assert (is_symmetric (construct [ 5; 3; 18; 1; 4; 12; 21 ]));
  assert (not (is_symmetric (construct [ 3; 2; 5; 7; 4 ])))
;;
