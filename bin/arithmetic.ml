(* 31. Determine whether a given integer number is prime. (medium) *)
let is_prime n =
  let rec aux i =
    if i * i > n then true else if n mod i = 0 then false else aux (i + 1)
  in
  if n < 2 then false else aux 2
;;

let () =
  assert (not (is_prime 1));
  assert (is_prime 7);
  assert (is_prime 2);
  assert (not (is_prime 1))
;;

(* 32. Determine the greatest common divisor of two positive integer numbers. (medium) *)
let rec gcd a b = if a = 0 then b else gcd (b mod a) a

let () =
  assert (gcd 13 27 = 1);
  assert (gcd 20536 7826 = 2)
;;

(* 33. Determine whether two positive integer numbers are coprime. (easy) *)
let coprime a b = gcd a b = 1

let () =
  assert (coprime 13 27);
  assert (not (coprime 20536 7826))
;;

(* 34. Calculate Euler's totient function φ(m). (medium) *)
let phi m =
  let rec aux acc i =
    if i >= m then acc else aux (if coprime i m then acc + 1 else acc) (i + 1)
  in
  aux 0 1
;;

let () =
  assert (phi 10 = 4);
  assert (phi 13 = 12)
;;

(* 35. Determine the prime factors of a given positive integer. (medium) *)
let factors n =
  let rec aux prime_factor n =
    if n = 1
    then []
    else if n mod prime_factor = 0
    then prime_factor :: aux prime_factor (n / prime_factor)
    else aux (prime_factor + 1) n
  in
  aux 2 n
;;

let () = assert (factors 315 = [ 3; 3; 5; 7 ])

(* 36. Determine the prime factors of a given positive integer (2). (medium) *)
let factors n =
  let rec aux prime_factor n =
    if n = 1
    then []
    else if n mod prime_factor = 0
    then (
      match aux prime_factor (n / prime_factor) with
      | (next_factor, quant) :: t when next_factor = prime_factor ->
        (next_factor, quant + 1) :: t
      | lst -> (prime_factor, 1) :: lst)
    else aux (prime_factor + 1) n
  in
  aux 2 n
;;

let () = assert (factors 315 = [ 3, 2; 5, 1; 7, 1 ])

let rec pow a = function
  | 0 -> 1
  | 1 -> a
  | n ->
    let b = pow a (n / 2) in
    b * b * if n mod 2 = 0 then 1 else a
;;

(* 37. Calculate Euler's totient function φ(m) (improved). (medium) *)
let phi_improved m =
  let rec aux acc = function
    | [] -> acc
    | (pi, mi) :: t -> aux (acc * (pi - 1) * pow pi (mi - 1)) t
  in
  aux 1 (factors m)
;;

let () =
  assert (phi_improved 10 = 4);
  assert (phi_improved 13 = 12)
;;

(* 38. Compare the two methods of calculating Euler's totient function. (easy) *)
let timeit f a =
  let start_time = Unix.gettimeofday () in
  let _ = f a in
  Unix.gettimeofday () -. start_time
;;

let () =
  Printf.printf "phi time: %.8f\n" (timeit phi 10090);
  Printf.printf "phi improved time: %.8f\n" (timeit phi_improved 10090)
;;

(* 39. A list of prime numbers. (easy) *)
let all_primes lower upper =
  let sieve = Array.init (upper + 1) (fun _ -> true) in
  let rec aux i acc =
    if i > upper
    then List.rev acc
    else if sieve.(i)
    then (
      mark (2 * i) i;
      let next_acc = if i >= lower then i :: acc else acc in
      aux (i + 1) next_acc)
    else aux (i + 1) acc
  and mark start step =
    if start > upper
    then ()
    else (
      sieve.(start) <- false;
      mark (start + step) step)
  in
  aux 2 []
;;

let () =
  assert (List.length (all_primes 2 7920) = 1000);
  assert (all_primes 3 17 = [ 3; 5; 7; 11; 13; 17 ])
;;

(* 40. Goldbach's conjecture. (medium) *)
let goldbach sum =
  let rec aux i = if is_prime i && is_prime (sum - i) then i, sum - i else aux (i + 1) in
  aux 2
;;

let () = assert (goldbach 28 = (5, 23))

(* 41. A list of Goldbach compositions. (medium) *)
let goldbach_list lower upper =
  let rec aux acc lower =
    if lower > upper
    then List.rev acc
    else if lower > 2 && lower mod 2 = 0
    then aux ((lower, goldbach lower) :: acc) (lower + 2)
    else aux acc (lower + 1)
  in
  aux [] lower
;;

let goldbach_limit lower upper min_both =
  List.filter
    (fun (_, (p1, p2)) -> p1 >= min_both && p2 >= min_both)
    (goldbach_list lower upper)
;;

let () =
  assert (
    goldbach_list 9 20
    = [ 10, (3, 7); 12, (5, 7); 14, (3, 11); 16, (3, 13); 18, (5, 13); 20, (3, 17) ]);
  assert (
    goldbach_limit 1 2000 50
    = [ 992, (73, 919); 1382, (61, 1321); 1856, (67, 1789); 1928, (61, 1867) ])
;;
