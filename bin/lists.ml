(* Problem 01 *)
let rec last = function
  | [] -> None
  | [ x ] -> Some x
  | _ :: t -> last t
;;

let () =
  assert (last [ "a"; "b"; "c"; "d" ] = Some "d");
  assert (last [] = None)
;;

(* Problem 02 *)
let rec last_two = function
  | [] | [ _ ] -> None
  | [ x; y ] -> Some (x, y)
  | _ :: t -> last_two t
;;

let () =
  assert (last_two [ "a"; "b"; "c"; "d" ] = Some ("c", "d"));
  assert (last_two [ "a" ] = None)
;;

(* Problem 03 *)
let rec at n = function
  | [] -> None
  | h :: _ when n = 1 -> Some h
  | _ :: t -> at (n - 1) t
;;

let () =
  assert (at 3 [ "a"; "b"; "c"; "d"; "e" ] = Some "c");
  assert (at 3 [ "a" ] = None)
;;

(* Problem 04 *)
let length lst =
  let rec aux acc = function
    | [] -> acc
    | _ :: t -> aux (acc + 1) t
  in
  aux 0 lst
;;

let () =
  assert (length [ "a"; "b"; "c" ] = 3);
  assert (length [] = 0)
;;

(* Problem 05 *)
let rev lst =
  let rec aux acc = function
    | [] -> acc
    | h :: t -> aux (h :: acc) t
  in
  aux [] lst
;;

let () = assert (rev [ "a"; "b"; "c" ] = [ "c"; "b"; "a" ])

(* Problem 06 *)
let is_palindrome lst = lst = rev lst

let () =
  assert (is_palindrome [ "x"; "a"; "m"; "a"; "x" ]);
  assert (not (is_palindrome [ "a"; "b" ]))
;;

(* Problem 07 *)
type 'a node =
  | One of 'a
  | Many of 'a node list

let flatten lst =
  let rec aux acc = function
    | [] -> acc
    | One h :: t -> aux (h :: acc) t
    | Many h :: t -> aux (aux acc h) t
  in
  rev (aux [] lst)
;;

let () =
  assert (
    flatten [ One "a"; Many [ One "b"; Many [ One "c"; One "d" ]; One "e" ] ]
    = [ "a"; "b"; "c"; "d"; "e" ])
;;

(* Problem 08 *)
let rec compress = function
  | [] -> []
  | [ x ] -> [ x ]
  | x :: (y :: _ as rest) -> if x = y then compress rest else x :: compress rest
;;

let () =
  assert (
    compress [ "a"; "a"; "a"; "a"; "b"; "c"; "c"; "a"; "a"; "d"; "e"; "e"; "e"; "e" ]
    = [ "a"; "b"; "c"; "a"; "d"; "e" ])
;;

(* Problem 09 *)
let pack list =
  let rec aux cur acc = function
    | [] -> [ acc ]
    | h :: t when h = cur -> aux h (h :: acc) t
    | h :: t -> acc :: aux h [ h ] t
  in
  aux (List.hd list) [] list
;;

let () =
  assert (
    pack [ "a"; "a"; "a"; "a"; "b"; "c"; "c"; "a"; "a"; "d"; "d"; "e"; "e"; "e"; "e" ]
    = [ [ "a"; "a"; "a"; "a" ]
      ; [ "b" ]
      ; [ "c"; "c" ]
      ; [ "a"; "a" ]
      ; [ "d"; "d" ]
      ; [ "e"; "e"; "e"; "e" ]
      ])
;;

(* 10. Run-length encoding of a list. (easy) *)
let encode lst =
  let rec aux n acc = function
    | [] -> acc
    | [ x ] -> (n + 1, x) :: acc
    | x :: (y :: _ as rest) ->
      if x = y then aux (n + 1) acc rest else aux 0 ((n + 1, x) :: acc) rest
  in
  rev (aux 0 [] lst)
;;

let () =
  assert (
    encode [ "a"; "a"; "a"; "a"; "b"; "c"; "c"; "a"; "a"; "d"; "e"; "e"; "e"; "e" ]
    = [ 4, "a"; 1, "b"; 2, "c"; 2, "a"; 1, "d"; 4, "e" ])
;;

type 'a rle =
  | One of 'a
  | Many of int * 'a

(* 11. Modified run-length encoding. (easy) *)
let encode lst =
  let rec aux n acc = function
    | [] -> acc
    | [ x ] -> one_or_many n x :: acc
    | x :: (y :: _ as rest) ->
      if x = y then aux (n + 1) acc rest else aux 0 (one_or_many n x :: acc) rest
  and one_or_many n x = if n = 0 then One x else Many (n + 1, x) in
  rev (aux 0 [] lst)
;;

let () =
  assert (
    encode [ "a"; "a"; "a"; "a"; "b"; "c"; "c"; "a"; "a"; "d"; "e"; "e"; "e"; "e" ]
    = [ Many (4, "a"); One "b"; Many (2, "c"); Many (2, "a"); One "d"; Many (4, "e") ])
;;

(* 12. Decode a run-length encoded list. (medium) *)
let decode lst =
  let rec aux acc = function
    | [] -> acc
    | h :: t -> aux (expand acc h) t
  and expand acc = function
    | One x -> x :: acc
    | Many (n, x) -> expand_many acc n x
  and expand_many acc n x = if n = 0 then acc else expand_many (x :: acc) (n - 1) x in
  rev (aux [] lst)
;;

let () =
  assert (
    decode
      [ Many (4, "a"); One "b"; Many (2, "c"); Many (2, "a"); One "d"; Many (4, "e") ]
    = [ "a"; "a"; "a"; "a"; "b"; "c"; "c"; "a"; "a"; "d"; "e"; "e"; "e"; "e" ])
;;

(* 13. Run-length encoding of a list (direct solution). (medium) *)
(* Same as 11 *)

(* 14. Duplicate the elements of a list. (easy) *)
let duplicate lst =
  let rec aux acc = function
    | [] -> acc
    | h :: t -> aux (h :: h :: acc) t
  in
  rev (aux [] lst)
;;

let () =
  assert (
    duplicate [ "a"; "b"; "c"; "c"; "d" ]
    = [ "a"; "a"; "b"; "b"; "c"; "c"; "c"; "c"; "d"; "d" ])
;;

(* 15. Replicate the elements of a list a given number of times. (medium) *)
let replicate lst n =
  let rec aux acc n' = function
    | [] -> acc
    | h :: t as all -> if n' = 0 then aux acc n t else aux (h :: acc) (n' - 1) all
  in
  rev (aux [] n lst)
;;

let () =
  assert (replicate [ "a"; "b"; "c" ] 3 = [ "a"; "a"; "a"; "b"; "b"; "b"; "c"; "c"; "c" ])
;;

(* 16. Drop every N'th element from a list. (medium) *)
let drop lst n =
  let rec aux n' = function
    | [] -> []
    | h :: t -> if n' <= 1 then aux n t else h :: aux (n' - 1) t
  in
  aux n lst
;;

let () =
  assert (
    drop [ "a"; "b"; "c"; "d"; "e"; "f"; "g"; "h"; "i"; "j" ] 3
    = [ "a"; "b"; "d"; "e"; "g"; "h"; "j" ])
;;

(* 17. Split a list into two parts; the length of the first part is given. (easy) *)
let split lst n =
  let rec aux acc n' = function
    | [] -> rev acc, []
    | h :: t as all -> if n' = 0 then rev acc, all else aux (h :: acc) (n' - 1) t
  in
  aux [] n lst
;;

let () =
  assert (
    split [ "a"; "b"; "c"; "d"; "e"; "f"; "g"; "h"; "i"; "j" ] 3
    = ([ "a"; "b"; "c" ], [ "d"; "e"; "f"; "g"; "h"; "i"; "j" ]));
  assert (split [ "a"; "b"; "c"; "d" ] 5 = ([ "a"; "b"; "c"; "d" ], []))
;;

(* 18. Extract a slice from a list. (medium) *)
let slice lst i k =
  let rec aux j = function
    | [] -> []
    | h :: t -> if j < i then aux (j + 1) t else if j <= k then h :: aux (j + 1) t else []
  in
  aux 0 lst
;;

let () =
  assert (
    slice [ "a"; "b"; "c"; "d"; "e"; "f"; "g"; "h"; "i"; "j" ] 2 6
    = [ "c"; "d"; "e"; "f"; "g" ])
;;

(* 19. Rotate a list N places to the left. (medium) *)
let rotate lst n =
  let rec aux i acc = function
    | [] -> rev acc, []
    | h :: t as all -> if i = 0 then rev acc, all else aux (i - 1) (h :: acc) t
  in
  let size = length lst in
  let n = if size = 0 then 0 else ((n mod size) + size) mod size in
  if n = 0
  then lst
  else (
    let right, left = aux n [] lst in
    left @ right)
;;

let () =
  assert (
    rotate [ "a"; "b"; "c"; "d"; "e"; "f"; "g"; "h" ] 3
    = [ "d"; "e"; "f"; "g"; "h"; "a"; "b"; "c" ]);
  assert (
    rotate [ "a"; "b"; "c"; "d"; "e"; "f"; "g"; "h" ] (-2)
    = [ "g"; "h"; "a"; "b"; "c"; "d"; "e"; "f" ])
;;

(* 20. Remove the K'th element from a list. (easy) *)
let rec remove_at n = function
  | [] -> []
  | h :: t -> if n = 0 then t else h :: remove_at (n - 1) t
;;

let () = assert (remove_at 1 [ "a"; "b"; "c"; "d" ] = [ "a"; "c"; "d" ])

(* 21. Insert an element at a given position into a list. (easy) *)
let rec insert_at el pos = function
  | [] -> [ el ]
  | h :: t -> if pos = 0 then el :: h :: t else h :: insert_at el (pos - 1) t
;;

let () =
  assert (insert_at "alfa" 1 [ "a"; "b"; "c"; "d" ] = [ "a"; "alfa"; "b"; "c"; "d" ]);
  assert (insert_at "alfa" 3 [ "a"; "b"; "c"; "d" ] = [ "a"; "b"; "c"; "alfa"; "d" ]);
  assert (insert_at "alfa" 4 [ "a"; "b"; "c"; "d" ] = [ "a"; "b"; "c"; "d"; "alfa" ])
;;

(* 22. Create a list containing all integers within a given range. (easy) *)
let range a b =
  let rec aux i j = if i <= j then i :: aux (i + 1) j else [] in
  if a < b then aux a b else rev (aux b a)
;;

let () =
  assert (range 4 9 = [ 4; 5; 6; 7; 8; 9 ]);
  assert (range 9 4 = [ 9; 8; 7; 6; 5; 4 ])
;;

(* 23. Extract a given number of randomly selected elements from a list. (medium) *)
let rand_select lst n =
  let rec take n acc = function
    | h :: t -> if n = 0 then h, acc @ t else take (n - 1) (h :: acc) t
    | _ -> failwith "Out of bounds"
  and random_index lst = Random.int (length lst)
  and pick_n_random n acc lst =
    if n = 0
    then acc
    else (
      let random_element, rest = take (random_index lst) [] lst in
      pick_n_random (n - 1) (random_element :: acc) rest)
  in
  pick_n_random n [] lst
;;

let () =
  (* Test doesnt work propperly because of RNG, but each generation gives a random list of size 3*)
  (* assert (rand_select [ "a"; "b"; "c"; "d"; "e"; "f"; "g"; "h" ] 3 = [ "g"; "d"; "a" ]); *)
  ()
;;

(* 24. Lotto: Draw N different random numbers from the set 1..M. (easy) *)
let lotto_select k n = rand_select (range 1 n) k
(* let () = assert (lotto_select 6 49 = [ 10; 20; 44; 22; 41; 2 ]) *)

(* 25. Generate a random permutation of the elements of a list. (easy) *)
let permutation lst =
  let size = length lst in
  if size = 0
  then []
  else (
    let random_indexes = lotto_select size size in
    let rec aux = function
      | [] -> []
      | idx :: t -> Option.get (at idx lst) :: aux t
    in
    aux random_indexes)
;;

let _ = permutation [ "a"; "b"; "c"; "d"; "e"; "f" ]

(* 26. Generate the combinations of K distinct objects chosen from the N elements of a list. (medium) *)
let rec extract k lst =
  if k <= 0
  then [ [] ]
  else (
    match lst with
    | [] -> []
    | h :: t ->
      let take_h = List.map (fun other_comb -> h :: other_comb) (extract (k - 1) t) in
      let skip_h = extract k t in
      take_h @ skip_h)
;;

let () =
  assert (
    extract 2 [ "a"; "b"; "c"; "d" ]
    = [ [ "a"; "b" ]
      ; [ "a"; "c" ]
      ; [ "a"; "d" ]
      ; [ "b"; "c" ]
      ; [ "b"; "d" ]
      ; [ "c"; "d" ]
      ])
;;

(* 27. Group the elements of a set into disjoint subsets. (medium) *)
(* As its supposed to be a set, there shouldn't have any duplicates *)
let group lst group_sizes =
  let rec aux set = function
    | [] -> [ [] ]
    | size :: rest_sizes ->
      let combinations = extract size set in
      List.concat
        (List.map
           (fun subgroup ->
             let rest = set_diff subgroup set in
             List.map (fun x -> subgroup :: x) (aux rest rest_sizes))
           combinations)
  and set_diff set1 set2 = List.filter (fun x -> not (List.mem x set1)) set2 in
  aux lst group_sizes
;;

let () =
  (* My implementation generates the same elements, but in different order *)
  assert (
    group [ "a"; "b"; "c"; "d" ] [ 2; 1 ]
    = [ [ [ "a"; "b" ]; [ "c" ] ]
      ; [ [ "a"; "b" ]; [ "d" ] ]
      ; [ [ "a"; "c" ]; [ "b" ] ]
      ; [ [ "a"; "c" ]; [ "d" ] ]
      ; [ [ "a"; "d" ]; [ "b" ] ]
      ; [ [ "a"; "d" ]; [ "c" ] ]
      ; [ [ "b"; "c" ]; [ "a" ] ]
      ; [ [ "b"; "c" ]; [ "d" ] ]
      ; [ [ "b"; "d" ]; [ "a" ] ]
      ; [ [ "b"; "d" ]; [ "c" ] ]
      ; [ [ "c"; "d" ]; [ "a" ] ]
      ; [ [ "c"; "d" ]; [ "b" ] ]
      ])
;;

(* 28. Sorting a list of lists according to length of sublists. (medium) *)
let length_sort lst =
  let addlen lst = length lst, lst in
  let assossiative_length = List.map addlen lst in
  let sorted =
    List.sort (fun (len_a, _) (len_b, _) -> compare len_a len_b) assossiative_length
  in
  List.map snd sorted
;;

let frequency_sort lst =
  let addlen lst = length lst, lst in
  let assossiative_length = List.map addlen lst in
  let length_freq = Hashtbl.create 16 in
  List.iter
    (fun (size, _) ->
      let cur_count = Hashtbl.find_opt length_freq size |> Option.value ~default:0 in
      Hashtbl.replace length_freq size (cur_count + 1))
    assossiative_length;
  let sorted =
    List.sort
      (fun (len_a, _) (len_b, _) ->
        compare (Hashtbl.find length_freq len_a) (Hashtbl.find length_freq len_b))
      assossiative_length
  in
  List.map snd sorted
;;

let () =
  assert (
    length_sort
      [ [ "a"; "b"; "c" ]
      ; [ "d"; "e" ]
      ; [ "f"; "g"; "h" ]
      ; [ "d"; "e" ]
      ; [ "i"; "j"; "k"; "l" ]
      ; [ "m"; "n" ]
      ; [ "o" ]
      ]
    = [ [ "o" ]
      ; [ "d"; "e" ]
      ; [ "d"; "e" ]
      ; [ "m"; "n" ]
      ; [ "a"; "b"; "c" ]
      ; [ "f"; "g"; "h" ]
      ; [ "i"; "j"; "k"; "l" ]
      ]);
  assert (
    frequency_sort
      [ [ "a"; "b"; "c" ]
      ; [ "d"; "e" ]
      ; [ "f"; "g"; "h" ]
      ; [ "d"; "e" ]
      ; [ "i"; "j"; "k"; "l" ]
      ; [ "m"; "n" ]
      ; [ "o" ]
      ]
    = [ [ "i"; "j"; "k"; "l" ]
      ; [ "o" ]
      ; [ "a"; "b"; "c" ]
      ; [ "f"; "g"; "h" ]
      ; [ "d"; "e" ]
      ; [ "d"; "e" ]
      ; [ "m"; "n" ]
      ])
;;

(* Where is 29 and 30 ???*)
