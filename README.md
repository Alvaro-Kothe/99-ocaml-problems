# 99 Problems in OCaml

My solutions to [99 Problems in OCaml](https://v2.ocaml.org/learn/tutorials/99problems.html).

## Quickstart

### Test solutions

For every solution there is an `assert` to test if the implementation is correct.
Use `dune` to run the solutions.

```console
$ dune exec bin/lists.exe
$ dune exec bin/binary_tree.exe
```

### Interact with the implementation

To interact with the implementation in `REPL` use

```console
$ dune utop
```

Import the code with `#use "bin/file.ml"`, for example to interact with the lists
implementation.

```ocaml
# #use "bin/lists.ml";;
# flatten [One "a"; Many [One "b"; Many [One "c" ;One "d"]; One "e"]];;
```
